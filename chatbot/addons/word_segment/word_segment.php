<?php
/**
 * www.aiseminar.cn
 * AISChatBot
 * Version: 1.0.1
 * File: word_segment/word_segment.php
 * Author: jink2005(ZhaoYong)
 * Date: 2013-10-16
 */

/**
 * function getSegmentedWords
 * A function to segment sentence to words array
 * @param string $sentence
 * @return array $wordArr
 */
function getSegmentedWords($sentence)
{
	$url = "http://open.vapsec.com/segment/get_word?word=".rawurlencode($sentence)."&format=string";
		
	$ch = curl_init();	
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );	
	$output = curl_exec($ch);
	curl_close($ch);
		
	$wordArr = explode('_', $output);	
	return $wordArr;
}

?>